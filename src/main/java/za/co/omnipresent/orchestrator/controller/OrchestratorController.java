package za.co.omnipresent.orchestrator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import za.co.omnipresent.orchestrator.models.*;
import za.co.omnipresent.orchestrator.service.OrchestratorService;

@RestController
@RequestMapping(value = "/orchestrate")
public class OrchestratorController {

    private OrchestratorService orchestratorService;

    @GetMapping(value = "/twitter/analysis/{topic}")
    public @ResponseBody OverallCountrySentiment callServices(@PathVariable String topic)
    {
        return orchestratorService.callServices(topic);
    }

    @Autowired
    public void setOrchestratotService(OrchestratorService orchestratorService){
        this.orchestratorService = orchestratorService;
    }

}
