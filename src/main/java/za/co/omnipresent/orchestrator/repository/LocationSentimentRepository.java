package za.co.omnipresent.orchestrator.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.omnipresent.orchestrator.models.LocationSentiment;

public interface LocationSentimentRepository extends MongoRepository<LocationSentiment, String> {
    LocationSentiment[] findByTopic(String topic);
}
