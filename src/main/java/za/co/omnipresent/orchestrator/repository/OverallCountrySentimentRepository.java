package za.co.omnipresent.orchestrator.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.omnipresent.orchestrator.models.OverallCountrySentiment;

public interface OverallCountrySentimentRepository extends MongoRepository<OverallCountrySentiment, String> {
    OverallCountrySentiment findByCountryNameAndTopic(String countryName, String topic);
}
