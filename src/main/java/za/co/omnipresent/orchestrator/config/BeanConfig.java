package za.co.omnipresent.orchestrator.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import za.co.omnipresent.orchestrator.service.OrchestratorServiceImpl;

@Configuration
public class BeanConfig {

    @Bean
    public RestTemplate getRestTemplate()
    {
        return new RestTemplate();
    }

    @Bean
    public OrchestratorServiceImpl orhcestratorService(){
        return new OrchestratorServiceImpl();
    }
}
