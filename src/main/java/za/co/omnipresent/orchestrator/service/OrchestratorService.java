package za.co.omnipresent.orchestrator.service;

import za.co.omnipresent.orchestrator.models.LocationSentiment;
import za.co.omnipresent.orchestrator.models.OverallCountrySentiment;

public interface OrchestratorService {
   OverallCountrySentiment callServices(String topic);
}
