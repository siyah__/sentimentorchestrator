package za.co.omnipresent.orchestrator.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import za.co.omnipresent.orchestrator.models.LocationSentiment;
import za.co.omnipresent.orchestrator.models.OverallCountrySentiment;
import za.co.omnipresent.orchestrator.models.TweetsWithAnalysis;
import za.co.omnipresent.orchestrator.repository.LocationSentimentRepository;
import za.co.omnipresent.orchestrator.repository.OverallCountrySentimentRepository;

@Log4j2
public class OrchestratorServiceImpl implements OrchestratorService {

    @Value("${twitter.search.url}")
    private String twitterSearchUrl;
    @Value("${text.analysis.url}")
    private String textAnalysisUrl;
    @Value("${aggregator.service.url}")
    private String aggregatorServiceUrl;
    private OverallCountrySentimentRepository overallCountrySentimentRepository;
    private RestTemplate restTemplate;

    @Override
    public OverallCountrySentiment callServices(String topic) {
        OverallCountrySentiment overallCountrySentiment = overallCountrySentimentRepository.findByCountryNameAndTopic("South Africa", topic);

        if(overallCountrySentiment != null){
            return overallCountrySentiment;
        }


        ResponseEntity<String> searchTweetsResponse = restTemplate.getForEntity(twitterSearchUrl.replace("{topic}", topic), String.class);
        log.info(searchTweetsResponse.getStatusCode().toString());

        ResponseEntity<TweetsWithAnalysis[]> textAnalysisResponse = restTemplate.getForEntity(textAnalysisUrl, TweetsWithAnalysis[].class);
        log.info(textAnalysisResponse.getStatusCode().toString());

        ResponseEntity<OverallCountrySentiment> aggregatorResponse = restTemplate.getForEntity(aggregatorServiceUrl.replace("{topic}", topic), OverallCountrySentiment.class);
        log.info(aggregatorResponse.getStatusCode().toString());

        return aggregatorResponse.getBody();
    }

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate)
    {
        this.restTemplate = restTemplate;
    }

    @Autowired
    public void setOverallCountrySentimentRepository(OverallCountrySentimentRepository overallCountrySentimentRepository) {
        this.overallCountrySentimentRepository = overallCountrySentimentRepository;
    }



}
