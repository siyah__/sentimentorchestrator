package za.co.omnipresent.orchestrator.models;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

@Data
public class Statuses implements Serializable {

    @Id
    private String id;

    private String topic;
    @JsonAlias({"created_at"})
    private String createdAt;
    @JsonAlias({"full_text"})
    private String fullText;

    public Statuses() {
    }
}