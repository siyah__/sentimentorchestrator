package za.co.omnipresent.orchestrator.models;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

@Data
public class LocationSentiment implements Serializable {
    @Id
    private String id;
    private String province;
    private String cityName;
    private String countryName;
    private String topic;
    private double overallSentiment;

    public LocationSentiment() {
    }

    public LocationSentiment(String province, String cityName, String topic, double overallSentiment, String countryName) {
        this.province = province;
        this.cityName = cityName;
        this.topic = topic;
        this.overallSentiment = overallSentiment;
        this.countryName = countryName;
    }
}
