package za.co.omnipresent.orchestrator.models;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.List;

@Data
public class OverallCountrySentiment {

    @Id
    private String id;
    private String countryName;
    private String topic;
    private double overallSentiment;
    private List<ProvinceSentiment> provinceSentiments;

    public OverallCountrySentiment(String countryName, String topic, double overallSentiment, List<ProvinceSentiment> provinceSentiments) {
        this.countryName = countryName;
        this.topic = topic;
        this.overallSentiment = overallSentiment;
        this.provinceSentiments = provinceSentiments;
    }
}
