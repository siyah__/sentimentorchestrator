package za.co.omnipresent.orchestrator.models;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class TweetsResponse implements Serializable {
    private List<Statuses> statuses;

    public TweetsResponse()
    {
        statuses = new ArrayList<>();
    }

}
