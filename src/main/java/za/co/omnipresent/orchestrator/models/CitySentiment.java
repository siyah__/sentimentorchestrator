package za.co.omnipresent.orchestrator.models;

import lombok.Data;

@Data
public class CitySentiment {
    private String cityName;
    private String provinceName;
    private String countryName;
    private double overallSentiment;

    public CitySentiment(String cityName, String provinceName, String countryName, double overallSentiment) {
        this.cityName = cityName;
        this.provinceName = provinceName;
        this.countryName = countryName;
        this.overallSentiment = overallSentiment;
    }
}
