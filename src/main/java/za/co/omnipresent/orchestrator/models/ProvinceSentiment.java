package za.co.omnipresent.orchestrator.models;

import lombok.Data;

import java.util.List;

@Data
public class ProvinceSentiment {
    private String provinceName;
    private String countryName;
    private double overallSentiment;
    private List<CitySentiment> citySentiments;

    public ProvinceSentiment(String provinceName, String countryName, double overallSentiment, List<CitySentiment> citySentiments) {
        this.provinceName = provinceName;
        this.countryName = countryName;
        this.overallSentiment = overallSentiment;
        this.citySentiments = citySentiments;
    }
}


