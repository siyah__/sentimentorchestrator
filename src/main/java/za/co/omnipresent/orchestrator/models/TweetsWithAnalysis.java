package za.co.omnipresent.orchestrator.models;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

@Data
public class TweetsWithAnalysis implements Serializable {
    @Id
    private String id;

    private String topic;
    private String fullText;
    private double magnitude;
    private double score;
    private String province;
    private String cityName;

    public TweetsWithAnalysis() {
    }

    public TweetsWithAnalysis(String topic, String fullText, double magnitude, double score, String province, String cityName){
        this.topic = topic;
        this.fullText = fullText;
        this.magnitude = magnitude;
        this.score = score;
        this.province = province;
        this.cityName = cityName;
    }
}
