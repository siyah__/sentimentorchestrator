package za.co.omnipresent.orchestrator.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;
import za.co.omnipresent.orchestrator.models.*;
import za.co.omnipresent.orchestrator.repository.OverallCountrySentimentRepository;
import java.util.ArrayList;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class OrchestratorServiceImplTest {
    private OrchestratorServiceImpl orchestratorService;
    private RestTemplate restTemplate;
    @Autowired
    private OverallCountrySentimentRepository overallCountrySentimentRepository;
    @Autowired
    private MongoTemplate mongoTemplate;

    @Before
    public void setUp(){
        orchestratorService = new OrchestratorServiceImpl();
        restTemplate = Mockito.mock(RestTemplate.class);
        MockitoAnnotations.initMocks(this);
        orchestratorService.setRestTemplate(restTemplate);
        orchestratorService.setOverallCountrySentimentRepository(this.overallCountrySentimentRepository);
        ReflectionTestUtils.setField(orchestratorService, "twitterSearchUrl", "http://localhost:8082/social/socials/twitter/search/{topic}");
        ReflectionTestUtils.setField(orchestratorService, "textAnalysisUrl", "http://localhost:8081/analysis/analyze/sentiment");
        ReflectionTestUtils.setField(orchestratorService, "aggregatorServiceUrl", "http://localhost:8083/aggregation/aggregator/aggregate/sentiment/{topic}");

    }

    @After
    public void clean(){
        mongoTemplate.dropCollection(OverallCountrySentiment.class);
    }

    @Test
    @DisplayName("Given a topic, when the db cant find the topic, then the services should be called and return a array of location sentiments")
    public void shouldCallServices(){
        saveOverallCountrySentiments();
        this.overallCountrySentimentRepository.deleteAll();

        Mockito.when(restTemplate.getForEntity("http://localhost:8082/social/socials/twitter/search/cats" ,String.class)).thenReturn(ResponseEntity.ok("Tweets retrieved and stored in the database."));
        Mockito.when(restTemplate.getForEntity("http://localhost:8081/analysis/analyze/sentiment" ,TweetsWithAnalysis[].class)).thenReturn(ResponseEntity.ok(returnTweetsWithAnalysisArray()));
        Mockito.when(restTemplate.getForEntity("http://localhost:8083/aggregation/aggregator/aggregate/sentiment/cats" ,OverallCountrySentiment.class)).thenReturn(ResponseEntity.ok(returnOverallCountrySentiment()));

        OverallCountrySentiment overallCountrySentiment = orchestratorService.callServices("cats");
        Assert.assertEquals("South Africa", overallCountrySentiment.getCountryName());
        Assert.assertEquals("Cars", overallCountrySentiment.getTopic());
        Assert.assertEquals(50.0, overallCountrySentiment.getOverallSentiment(), 0);
        Assert.assertFalse(overallCountrySentiment.getProvinceSentiments().isEmpty());
        Assert.assertTrue(overallCountrySentiment.getProvinceSentiments().get(0).getCitySentiments().size() > 1);
    }

    @Test
    @DisplayName("Given a topic, when the db finds the topic, then it will return an array of location sentiments, without calling services.")
    public void shouldNotCallServices(){
        saveOverallCountrySentiments();
        OverallCountrySentiment overallCountrySentiment = orchestratorService.callServices("Cars");
        Assert.assertEquals("South Africa", overallCountrySentiment.getCountryName());
        Assert.assertEquals("Cars", overallCountrySentiment.getTopic());
        Assert.assertEquals(50.0, overallCountrySentiment.getOverallSentiment(), 0);
        Assert.assertFalse(overallCountrySentiment.getProvinceSentiments().isEmpty());
        Assert.assertTrue(overallCountrySentiment.getProvinceSentiments().get(0).getCitySentiments().size() > 1);
    }
    public void saveOverallCountrySentiments (){
        List<ProvinceSentiment> provinceSentiments = createProvinceSentimentsList();

        this.overallCountrySentimentRepository.save(new OverallCountrySentiment("South Africa", "Cars", 50.00, provinceSentiments));
    }

    public OverallCountrySentiment returnOverallCountrySentiment(){
        List<ProvinceSentiment> provinceSentiments = createProvinceSentimentsList();
        return new OverallCountrySentiment("South Africa", "Cars", 50.00, provinceSentiments);
    }

    public List<ProvinceSentiment> createProvinceSentimentsList(){
        List<ProvinceSentiment> provincesList = new ArrayList<>();
        provincesList.add(new ProvinceSentiment("Gauteng", "Cars", 60.00, createCitySentimentsList()));
        provincesList.add(new ProvinceSentiment("Gauteng", "South Africa", 60.00, createCitySentimentsList1()));
        return provincesList;
    }
    public List<CitySentiment> createCitySentimentsList(){
        List<CitySentiment> citiesList = new ArrayList<>();
        citiesList.add(new CitySentiment("Johannesburg", "Gauteng", "South Africa", 20.00));
        citiesList.add(new CitySentiment("Pretoria", "Gauteng", "South Africa", 20.00));

        return citiesList;
    }
    public List<CitySentiment> createCitySentimentsList1(){
        List<CitySentiment> citiesList = new ArrayList<>();
        citiesList.add(new CitySentiment("Stellenbosch", "Western Cape", "South Africa", 20.00));
        citiesList.add(new CitySentiment("Cape Town", "Western Cape", "South Africa", 20.00));

        return citiesList;
    }
    private TweetsWithAnalysis[] returnTweetsWithAnalysisArray(){
        TweetsWithAnalysis[] tweets = new TweetsWithAnalysis[2];
        tweets[0] = new TweetsWithAnalysis("cars", "I love cars so much", 0.5, 0.5, "Gauteng", "Johannesburg");
        tweets[1] = new TweetsWithAnalysis("cats", "I love cats so much", 0.5, 0.5, "Gauteng", "Johannesburg");
        return tweets;
    }
}
